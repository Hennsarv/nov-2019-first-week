﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paarilised // kolmikud, nelikud vms
{
    class Program
    {
        static void Main(string[] args)
        {
            string nimi = "Henn";
            int vanus = 64;
            Console.WriteLine($"{nimi} on {vanus} aastane");
            (nimi, vanus) = ("Ants", 40); // nimi = "Ants"; vanus = 40;
            Console.WriteLine($"{nimi} on {vanus} aastane");

            // jätaks need mitmikud natukeseks sinnapaika


            // massiivid? head või halvad?
            // massivi puhul - kindel suurus - hea ja halb omadus

            List<int> arvud = new List<int>(100) { 17, 11, 23 };

            arvud.Add(7); Console.WriteLine(arvud.Capacity);
            arvud.Add(3); Console.WriteLine(arvud.Capacity);
            arvud.Add(2); Console.WriteLine(arvud.Capacity);
            arvud.Add(8); Console.WriteLine(arvud.Capacity);

            for (int i = 0; i < 10; i++)
            {
                arvud.Add(i); Console.WriteLine(arvud.Capacity);

            }

            Console.WriteLine(arvud[3]);  // pakume - 8
            arvud.Remove(3); Console.WriteLine(arvud.Capacity);
            foreach (int x in arvud) Console.Write($"{x} "); // 
            Console.WriteLine();

            int[] massiiv = arvud.ToArray();
            List<int> teine = massiiv.ToList();


            for (int i = 0; i < arvud.Count; i++) Console.WriteLine($"listi kohal {i} on {arvud[i]}");

            List<int> numbrid = Enumerable.Range(1, 100).ToList();

            Dictionary<string, int> vanused = new Dictionary<string, int>();

            vanused.Add("Henn", 64);
            vanused.Add("Ants", 40);
            vanused.Add("Peeter", 28);

            vanused["Ants"] += 8;
            vanused["Malle"] = 22;

            Console.WriteLine(vanused["Peeter"]);

            if (vanused.ContainsKey("Malle")) Console.WriteLine("Malle vanust me teame");

            foreach (string key in vanused.Keys) Console.WriteLine($"{key} vanus on {vanused[key]}");

            Console.WriteLine($"keskmine vanus on {vanused.Values.Average()}");

            foreach (var x in vanused) Console.WriteLine(x);

            SortedSet<int> numbridSorted = new SortedSet<int> { 7, 1, 2, 8, 3, 9,4, 4,  0 };
            numbridSorted.Add(3);
            foreach (var x in numbridSorted) Console.Write($"{x} ");
            Console.WriteLine();
            

            SortedSet<string> nimekiri = new SortedSet<string> { "Henn", "Ants", "Peeter", "ANTS", "Volli", "Wolli", "Villu", "Willu" };
            foreach (var x in nimekiri) Console.Write($"{x} ");
            Console.WriteLine();



        }

        static int Summa(int[] arvud)
        {
            int summa = 0;
            foreach (int x in arvud) summa += x;
            return summa;
        }

        static int Loendus(int[] arvud)
        {
            int summa = 0;
            foreach (int x in arvud) summa++;
            return summa;
        }

        static (int,int) Arvutus (int[] arvud)
        {
            (int summa, int loendur) = (0, 0);
            foreach (int x in arvud) (summa, loendur) =(summa + x, loendur+1);
            return (summa, loendur);
        }



    }



}
