﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnVar
{
    class Program
    {
        static void Main(string[] args)
        {
            // seletame veel ühe kasuliku asja nimega var

            var arv = 7.0;
            Console.WriteLine(arv.GetType().Name);

            var henn = new { Nimi = "Henn", Vanus = 64 };
            Console.WriteLine(henn.GetType().Name);
            

        }
    }
}
