﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reede
{
    class Program
    {

        static void Main(string[] args)
        {
            // muutujad ja andmetüübid on selged
            // muutujal on neli asja (nimi, tüüp, väärtus, skoop)

            DateTime täna = DateTime.Today;
            DateTime henn = new DateTime(1955, 3, 7);
            var vahe = täna - henn;
            Console.WriteLine(vahe.Days);               // hennu elatud päevade arv
            Console.WriteLine(vahe.Days * 4 / 1461);        // hennu elatud täisaastaid

            Console.WriteLine(täna.Year - henn.Year);   // hennu sünniaasta ja tänavuse aasta vahe

            // vanus kuudes

            // if ei ole ju nii segane asi

            if (täna.DayOfWeek == DayOfWeek.Friday)
            {
                // jahh osa, mis tehakse kui tingimus on jahh (true)
            }
            else
            {
                // ei osa, mis tehakse kui tingimus on ei (false)
            }

            // millal saab kasutada elvist
            //  kui mõlemad (nii jahh kui ei osa) on 1-lausega omistamisavaldised
            // mis omistavad samale muutujale mingi väärtuse
            // või on sama tegevus, mis kasutab erinevat väärtus

            Console.Write("Palju sa palka saad: ");
            int palk = 0; // int.Parse(Console.ReadLine());
            if (palk > 1000)
                Console.WriteLine("läheme jooma");
            else
                Console.WriteLine("läheme jalutama");

            Console.WriteLine(palk > 1000 ? "läheme jooma" : "läheme jalutama");

            int maksud = palk < 500 ? 0 : (palk - 500) / 5;

            if (palk < 500) { maksud = 0; } else { maksud = (palk - 500) / 5; }

            Console.WriteLine("\nTere Henn!\nräägime, mis on write ja mis writeline");

            Console.WriteLine("Henn");
            Console.WriteLine("on");
            Console.WriteLine("ilus poiss");
            Console.Write("Henn");
            Console.Write("on");
            Console.Write("ilus poiss");
            Console.WriteLine();
            Console.Write("Henn palk on ");
            Console.Write(palk);
            Console.WriteLine(". Seda pole just palju");

            Console.WriteLine($"Hennu palk on \n{palk}.\n Seda pole just palju");

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Friday:
                case DayOfWeek.Tuesday:
                    // reedesed ja teisipäevased toimingud
                    break;

                case DayOfWeek.Sunday:
                    // laupäevane ja pühapäevane
                    break;

                case DayOfWeek.Saturday:
                    // laupäevane toiming
                    goto case DayOfWeek.Sunday;

                case DayOfWeek.Monday:
                    // esmaspäevased toimingud
                    goto default;

                default:
                    // kõigi muude päeade toimingud
                    break;

            }


            int[] arvud1 = new int[10]; // see on muutuja, mis sisaldab 10 int-i (vaikimisi kõik nullid)

            int[] arvud2 = new int[5] { 7, 3, 2, 9, 6 }; // see on muutuja, mis sisaldab 5 arvu - 7, 3, 2, 9, 6

            int[] arvud3 = { 1, 2, 7 }; // see on muutuja, mis sisaldab 3 arvu - 1, 2, 7
            string[] nimed =
            {
                "Henn",
                "Ants",
                "Peeter",
                "Joosep",
                "Malle",
                "Ülle",
            };

            var inimene = new { Nimi = "Henn", Vanus = 64 };

            Dictionary<string, int> ages = new Dictionary<string, int>
            {
                {"Henn", 64 },
            };

            // see lõik paneb tegelase jänene hüppama
            for (int jänene = 0; jänene < 10; jänene++)
            {
                Console.WriteLine($"jänene hüppab {jänene+1}. korda");
            }


            string testSplit = "Henn on ilus poiss";        // string muutuja (või avaldis)

            string[] splititud = testSplit
                .Split(' ');      // kui selle "otsa" kirjutada Split-funktsioon
                                  // tulemsueks stringimassiv
            splititud[0] = "Ants";
                                                                        
            testSplit = string.Join(" ", splititud);        // see paneb tükeldatud "massiivi" tagasi stringiks
            Console.WriteLine(testSplit);

            // see Parse ja TryParse tekitas eile veel peavalu?


            // int.Parse("4723")     // -> teisendab TEKSTI arvuks int 4723             kaval, tark ja viisakas progeja
            // Convert.ToInt32("4723")  // teeb TÄPASELT sama                           laisk, googli-usku, mujalt keelest tulnu

            Console.Write("Nimi ja palk palun: ");
            //palk = int.Parse(Console.ReadLine()); // loeb ekraanilt teksti ja teisendab

            if (! int.TryParse(         // jahh (kui teisendus õnnestu) ei (kui miski oli valesti)
                    Console.ReadLine(), // string avaldis
                    out palk)           // koht, kuhu kirjutada töö tulemus
                ) 
             { palk = 0; }





        }
    }
}
