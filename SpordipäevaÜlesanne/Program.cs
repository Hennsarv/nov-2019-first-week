﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SpordipäevaÜlesanne
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "..\\..\\protokoll.txt";

            string[] loetudRead = File.ReadAllLines(filename);

            double parimKiirus = 0;
            string kiireimJooksja = "";

            //foreach(string üksRida in loetudRead)
            for (int i = 1; i < loetudRead.Length; i++)
            {
                string üksRida = loetudRead[i];
                string[] reaOsad = üksRida.Split(',');
                //Console.WriteLine($"Nimi {reaOsad[0]}  distants{reaOsad[1]} aeg{reaOsad[2]}");
                string nimi = reaOsad[0];
                double distants = int.Parse(reaOsad[1]);
                //double aeg = int.Parse(reaOsad[2]);
                double aeg = int.TryParse(reaOsad[2], out int a) ? a : 0;
                double kiirus = distants / aeg;
                if (aeg != 0 && kiirus > parimKiirus)
                {
                    parimKiirus = kiirus;
                    kiireimJooksja = nimi;
                }
                Console.WriteLine($"{nimi} jooksis kiirusega {kiirus}");
            }
            Console.WriteLine($"kiireim jooksja oli {kiireimJooksja} tema kiiruseks mõõdeti {parimKiirus}");

        }
    }
}
