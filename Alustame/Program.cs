﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alustame
{
    class Program
    {
        static void Main(string[] args)   // see on põhimeetod, mis käima pannakse
        {
            Console.WriteLine("Tere Imelist hommikut!");

            // üks väike vidin veel, mida progemises vaja läheb
            // see on kommentaar

            // MUUTUJA
            // andmetüüp
            // avaldis

            int seeArv = 7;

            int teineArv = 3;

            Console.WriteLine(seeArv % teineArv);

            int suurArv = int.MaxValue;
            Console.WriteLine(suurArv + 10);

            char ahaa = 'a';
            Console.WriteLine(ahaa+0);
            ahaa++;
            Console.WriteLine(ahaa);

            string nimi = "Henn Sarv";
            
            Console.WriteLine(nimi);


            DateTime täna = DateTime.Now;
            Console.WriteLine(täna.ToString("dddd"));

            // tehted, avaldised ja teisendused

            int x = 0xfe01c2;
            Console.WriteLine(x);

            x = 0b110_1101_0110;
            string imeliktest = "esimene\nteine\nkolmas";
            Console.WriteLine(imeliktest);

            Console.WriteLine("Mida see kurat trükib\rSeda");
            Console.WriteLine("a\tb\tc\te");
            Console.WriteLine("aa\tbb\tcc\tee");

            Console.WriteLine("\nsiit hakkavad tehted\n");

            int a = 7;
            int b = 8;


            Console.WriteLine("tulemus on {0}", a += b);
            Console.WriteLine("a on {0}", a);

            a = 10;
            Console.WriteLine(a++);
            Console.WriteLine("a on {0}", a);
            a = 10;
            Console.WriteLine(++a);
            Console.WriteLine("a on {0}", a);


            Console.WriteLine(
                DateTime.Now.DayOfWeek == DayOfWeek.Sunday ? "täna puhkame" :
                DateTime.Now.DayOfWeek == DayOfWeek.Saturday ? "täna teeme sauna" :
                DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "täna läheme sulgpalli mängima" :
                                                            "täna ei puhka"
                
                
                );

            Console.WriteLine("\nNatuke ka teisendustest\n");

            decimal d = 10000;
            int i = (int)d;

            char c = 'c';
            c -=(char)10 ;
            Console.WriteLine(c);

            Console.Write("Vana sa oled: ");
            string vanus = Console.ReadLine();
            //int uusvanus = int.Parse(vanus);
            
            if(int.TryParse(vanus, out int uusvanus))
            {
                Console.WriteLine("ahha järgmine aasta oled juba {0} aatane", uusvanus + 1);
            }
            else
            {
                Console.WriteLine("see pole miski vanus");
            }

            Console.Write("millal sa sündisid: ");
            if (DateTime.TryParse(Console.ReadLine(), out DateTime sa))
            {
                Console.WriteLine("sa oled vist {0} aatat vana", (DateTime.Now - sa).Days * 4 / 1461);
            }
            else
            {
                Console.WriteLine("kuupäeva sa ka ei oska kirjutada");
            }

            // võrdleme nimesid

            string nimi1 = "Henn Sarv";
            string nimi2 = "Henn";
            nimi2 += " ";
            nimi2 += "Sarv";

            Console.WriteLine(
                nimi1 == nimi2 ? "nimed on samad" : "nimed ei ole samad"
                );
            
            if(decimal.TryParse("7,105", out decimal decim))
            {
                Console.WriteLine("tulemus on {0}", decim);
            }
            else
            {
                Console.WriteLine("ei õnnestunud");
            }


            

            //Console.WriteLine("press any key ...");
            //Console.ReadKey();
        }




        // järgnev on dokumentatsiooni-kommentaar, mis 1. eraldatakse dokumentatsioonifaili
        // ja 2. aitab "etteütlejat" sulle "ette ütelda"


        /// <summary>
        /// See on liitmise funktsioon
        /// </summary>
        /// <param name="yks">esimene liidetav</param>
        /// <param name="teine">teine liidetav</param>
        /// <returns></returns>
        static int Liida(int yks, int teine)
        {
            return yks + teine;
        }

    }
}
