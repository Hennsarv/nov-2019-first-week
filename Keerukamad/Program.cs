﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keerukamad
{
    class Program
    {
        static void Main(string[] args)
        {

            #region hommikused
            //Console.WriteLine("mingi jutt");
            //Console.WriteLine();
            //int x = 7;

            //Console.WriteLine("Muutuja x on {0:F2} ja tema ruut on {1:F2}", x, x*x);   // placeholderitega                  variant 1
            //Console.WriteLine("Muutuja x on " + x.ToString("F2") + " ja tema ruut on " + (x * x).ToString("F2"));       //  variant 2
            //Console.WriteLine($"Muutuja x on {x:F2} ja tema ruut on {x * x:F2}");      // interpoleeritud string            variant 3

            //// üks variant veel

            //string s;
            //s = String.Format("Muutuja x on {0:F2} ja tema ruut on {1:F2}", x, x * x); Console.WriteLine(s);                // 1
            //s = "Muutuja x on " + x.ToString("F2") + " ja tema ruut on " + (x * x).ToString("F2"); Console.WriteLine(s);    // 2
            //s = $"Muutuja x on {x:F2} ja tema ruut on {x * x:F2}"; Console.WriteLine(s);                                    // 3

            // variandi 3 teeb translaator sinu eest variandiks 1
            // varianti 2 ära kunagi kasuta


            //Console.Write("Kes sa oled: ");
            //string nimi = Console.ReadLine();
            //Console.Write("ja kui vana sa oled :");
            //int vanus = int.Parse(Console.ReadLine());
            //Console.WriteLine($"Große tag is heute. Begrüß dich {nimi}, do bist {vanus} Jahre alt");

            //string hennuNimi = "   Henn   sarv   ";
            //hennuNimi = hennuNimi.Trim();
            //Console.WriteLine(hennuNimi.ToUpper());
            //Console.WriteLine(hennuNimi.ToLower());
            //Console.WriteLine(hennuNimi.Length);
            //Console.WriteLine(hennuNimi.Substring(0, 4));   // sellest ma natuke räägin
            #endregion

            // esimene keerukam lause - tingimuslause if

            Console.WriteLine("Kes sa oled: ");
            string nimi = Console.ReadLine();
            if (nimi == "Henn")
            {
                Console.WriteLine("oi tere Henn");

            }
            else
            {
                Console.WriteLine($"no ok - ole siis {nimi}");
            }
            if (DateTime.Now.DayOfWeek != DayOfWeek.Saturday) Console.WriteLine("täna sauna ei saa");

            if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
            {
                // siia pühapäevased asjad
            }
            else if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            {
                // siia laupäevased asjad
            }
            else
            {
                // siia muud asjad
            }

            // proovi palun ise ka 
            // tee programmike, mis küsib consolilt valgusvoori värvi
            // kui see on "roheline" ütleb, et sõida edasi
            // kui see on kõllane ütleb, et oota kuni tuleb roheline
            // kui see on punane, ütleb et jää seisma

            // 1. küsin ekraanil, mis värvi tuli on
            // 2. loen konsoolilt vastuse
            // 3. analüüsin vastust if-lausetega


            // allpool on stringivõrdluse erinevaid variante kasutatud
            Console.Write("Mis värvi tuli seal on?: ");
            string tuli = Console.ReadLine().ToLower();
            if (tuli.Equals("punane", StringComparison.CurrentCultureIgnoreCase)) Console.WriteLine("jää seisma");
            else if ( new string[] { "roheline","green","grüne"}.Contains(tuli) ) Console.WriteLine("sõida edasi");
            else if (tuli == "kollane" || tuli == "kõllane") Console.WriteLine("oota rohelist");
            else Console.WriteLine("osta värviprillid");

            // teeme sama asja teist moodi - switch

            switch (tuli)
            {
                case "green":
                case "roheline":
                    Console.WriteLine("sõida edasi");
                    break;
                case "red":
                    Console.WriteLine("jää seisma");
                    goto case "yellow";
                case "yellow":
                    Console.WriteLine("oota rohelist");
                    break;
                default:
                    Console.WriteLine("osta värviprillid");
                    break;

            }

            // siin näide, mida ei saa switchiga teha
            Console.Write("Palju sa palka saad");
            int palk = int.Parse(Console.ReadLine());
            if (palk > 10000) Console.WriteLine("sinule võiks mehele minna");
            else if (palk > 1000) Console.WriteLine("vii mind siis restorani");
            else if (palk > 100) Console.WriteLine("lähem siis kinno");
            else Console.WriteLine("eks me pea siis pargis jalutama");



            //Console.ReadKey();
        }
    }
}
