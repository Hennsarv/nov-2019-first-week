﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassiividJaTsyklid
{
    class Program
    {
        static void Main(string[] args)
        {

            // eile vaatasime muutujat ja andmetüüpi

            int arv = 78;   // siin lauses on KOLM komponenti:
                            // 1. muutuja nimi arv                              kirjeldav osa
                            // 2. tema andmetüüp - int (või System.Int32)       kirjeldav osa
                            // 3. väärtustamisavaldis: = 78                     täidetav osa

            // muutuja peab ENNE kasutamist (mõnes avaldises või üksi) olema enne defineeritud ja saanud väärtuse
            // välja arvatud omistamisavaldise vasakul pool - siis ta peab enne olema lihtsalt defineeritud

            int[] arvud;    // tegemist on int-ide massiiviga, kus on neid PALJU (huvitav, kui palju)

            arvud = new int[10];
            arvud[7] = 78;
            Console.WriteLine(arvud[4]);

            int[] teine =  { 1, 2, 37, 4,5} ;

            // C# keeles on veel kaks massivi liiki

            int[][] ohoo = new int[10][];               // massiivide massiiv
            ohoo[7] = new int[] { 1, 2, 3, 4, 5, 6 };

            int[,] ahaa = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } }; // kahemõõtmeline massiv

            Console.WriteLine(ohoo[7][3]);
            Console.WriteLine(ahaa[2,1]);


            // nüüd saame õppuida selgeks uue viguri - tsüklid
            Console.WriteLine();

            for (int i = 0; i < arvud.Length; i++)
            {
                arvud[i] = i * i;
            }

            //for (int i = arvud.Length - 1; i >= 0; i--)
            //{
            //    if (i < 0) break;

            //    Console.WriteLine(arvud[i]);
            //}



            foreach (int x in arvud) Console.WriteLine(x);

    

            int ii = 0;

            while (ii < arvud.Length)
            {
                Console.WriteLine(arvud[ii++]);
            }

            //for (; ii < arvud.Length;)
            //{
            //    Console.WriteLine(arvud[ii++]);
            //}

            // sina proovi nüüd ise
            // tee meie eelmine valgusfooriprogramm ringi (või uuesti)
            // sealt valgusfoorist küsitakse ikka, mis värvi tuli seal on
            // aga seni kuni tuleb roheline, siis võib edasi sõita ja pole enam vaja küsida

            bool kasRoheline = false;
            while(!kasRoheline)
            {
                Console.Write("no mis värvi tuli seal on: ");
                switch(Console.ReadLine().ToLower())
                {
                    case "punane":
                        Console.WriteLine("seisa ja oota rohelist");
                        break;
                    case "kollane":
                        Console.WriteLine("oota rohelist");
                        break;
                    case "roheline":
                        Console.WriteLine("sõida edasi");
                        kasRoheline = true;
                        break;
                    default:
                        Console.WriteLine("osta värviprillid");
                        break;
                }
            }

            Random r = new Random();
            int size = r.Next(5,20);
            int[] mass = new int[size];
            for (int i = 0; i < size; i++)
            {
                mass[i] = r.Next(100);
                Console.WriteLine($"{i:00} on {mass[i]}");
            }

            int mitmes = 0;
            
            for (int i = 1; i < size; i++)
            {
                if (mass[i] > mass[mitmes]) mitmes = i;
            }
            Console.WriteLine($"kõige suurem arv on {mass[mitmes]} ja ta on kohal {mitmes}");

            int summa = 0;
            foreach (var x in mass) summa += x;
            Console.WriteLine($"keskmine on {(double)summa/size}");

            Console.WriteLine(mass.Average());

                       

        }
    }

    
}











