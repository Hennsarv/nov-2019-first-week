﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassiivistOtsimine
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] mass = new int[10, 10];
            Random r = new Random();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    mass[i, j] = r.Next(100);
                }
            }
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Console.Write($"\t{mass[i,j]}");
                }
                Console.WriteLine();
            }
            Console.Write("midaotsime: ");
            int otsime = int.Parse(Console.ReadLine());

            bool kasLeidsin = false;  // see muutuja hoiab meeles, kas ma olen juba leidnud mõne
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (mass[i, j] == otsime)
                    {
                        Console.WriteLine($"leidsin reast {i + 1} veerust {j + 1}");
                        kasLeidsin = true; // ja siin ütlen, kui ma olen leidnud mõne
                    }
                }
            }
            if (kasLeidsin == false)  Console.WriteLine("sihukest ei leidnud");

            //for (int i = 0; i < 100; i++)
            //{
            //    if (mass[i / 10,i%10] == otsime)
            //    {
            //        Console.WriteLine($"leidsin reast {i/10+1} ja veerust {1%10+1}");
            //        break;
            //    }
            //}




            // mul peaaegu toimib - aga
            // kuidas anda teada, et EI leidnud
            // kuidas anda vastuseks AINULT esimene

        }
    }
}
