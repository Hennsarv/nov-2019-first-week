﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaardipakk
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] pakk = Enumerable.Range(0, 52).ToArray();


            Console.WriteLine("\nsegamata pakk\n");
            for (int i = 0; i < 52; i++) Console.Write($"\t{pakk[i]}" + (i%4==3?"\n":""));


            Random r = new Random();

            
            Console.WriteLine("\njuhuslik pakk\n");

            for (int i = 0; i < 52; i++) pakk[i] = r.Next(52);
            for (int i = 0; i < 52; i++) Console.Write($"\t{pakk[i]}" + (i % 4 == 3 ? "\n" : ""));
            
            Console.WriteLine("\nteine juhuslikult täidetud pakk pakk\n");

            int?[] pakk2 = new int?[52];
            int v = 0;
            for (int i = 0; i < 52; ) if (!pakk2.Contains(v = r.Next(52))) pakk2[i++] = v;

            for (int i = 0; i < 52; i++) Console.Write($"\t{pakk2[i]}" + (i % 4 == 3 ? "\n" : ""));

            Console.WriteLine("\nsorteerimisega pakk\n");

            // see variant on alatu, sest me pole sihukest asja õppinud
            pakk = Enumerable.Range(0, 52)
                .OrderBy(x => r.NextDouble())
                .ToArray();
            for (int i = 0; i < 52; i++) Console.Write($"\t{pakk[i]}" + (i % 4 == 3 ? "\n" : ""));

        }


    }
}
