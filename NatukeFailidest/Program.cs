﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;        // see rida annab mulle lihtsa ligipääsu asjadele, mis oskavad failidega toimetada


//using static System.Console; // üks veider asi, mida vahel kasutatakse, aga enamasti mitte
// siin on oluline sõna (mille tähendust me veel ei tea) - static
// see võimaldab "hoida kokku" natuke juttu : )
// using static System.Math;

namespace NatukeFailidest
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\Nimekiri.txt";

            //Console.Write("Kes sa oled: ");
            //var nimi = Console.ReadLine();
            //Console.WriteLine($"Tere {nimi}!");

            //var nimekiri1 = File.ReadAllText(@"c:\henn\nimekiri.txt");
            //Console.WriteLine(nimekiri1);
            //Console.WriteLine("-------------------");

            var nimekiri = File.ReadAllLines(filename);   // loeb faili ja paneb selle sisu stringimassiivi
            foreach (var x in nimekiri) Console.WriteLine(x);
            Console.WriteLine("-------------------");

            Dictionary<string, int> vanused = new Dictionary<string, int>();
            foreach (var rida in nimekiri)
                if (rida != "") // jätab arvest välja kõik tühjad read
            {
                var osad = rida.Split(',');
                    if (osad.Length > 1) // jätab arvest välja need, milles koma ei ole
                        // vanused.Add(osad[0], int.Parse(osad[1]));
                        vanused.Add(osad[0], int.TryParse(osad[1], out int vanus) ? vanus : 0);

            }

            foreach (var x in vanused) Console.WriteLine(x);

            

        }
    }
}










