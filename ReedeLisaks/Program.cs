﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReedeLisaks
{

    // meenutagem korraks, mis asi on "programm" 
    // see on miski kogus KLASSE, millest ühe sees on Main-meetod


    [Flags] enum Tunnused { Punane = 1, Puust = 2, Suur = 4, Kolisev = 8 }    

    class Program
    {
        static void Main(string[] args)
        {
            // kaks asja õpetan meile veel, siis lihstam toimetada
            // enum ja nullable

            Console.WriteLine("Esiteks vaatame, mis asi on enum\n");


            Tunnused tunnus = (Tunnused)9;
            tunnus |= Tunnused.Suur;                // lisada tunnus
            tunnus ^= Tunnused.Punane;              // ümber lülitada tunnus   
            Console.WriteLine(tunnus);

            if ((tunnus & Tunnused.Punane) == Tunnused.Punane) { } // kontrollida tunnust

            DayOfWeek päev = DayOfWeek.Friday;
            päev += 3;
            Console.WriteLine(päev);


            // teine vigur, mida meil vaja läheb, on nullable-tüüpi asjad


            int? vanus = 64;

            vanus = null;  // 0     null
            Console.WriteLine(vanus.HasValue);

            var tegelikvanus = vanus.HasValue ? vanus.Value : 0; // kas see on lihtne kirjaviis?
            tegelikvanus = vanus ?? 0;      // coalesce operatsioon


            Console.WriteLine(vanus?.ToString()??"väärtus puudub");



        }
    }

    class Inimene
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"{Nimi} vanusega {Vanus}";

    }
}
