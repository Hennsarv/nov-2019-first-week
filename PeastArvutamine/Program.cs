﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeastArvutamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            int punkte = 50;
            for (int test = 0; test < 10; test++)
            {
                int üks = r.Next(100);
                int teine = r.Next(100);
                int vastus = 0;
                string tehe = "";
                //if (r.Next(2) == 0) (vastus, tehe) = (üks + teine, "summa");
                //{
                //    vastus = üks + teine;
                //    tehe = "summa";
                //}
                //else (vastus, tehe) = (üks * teine, "korrutis");
                //{
                //    vastus = üks * teine;
                //    tehe = "korrutis";
                //}
                (vastus, tehe) = r.Next(2) == 0 ? (üks + teine, "summa") : (üks * teine, "korrutis");

                for (int katse = 0; katse < 5; katse++)
                {
                    Console.Write($"leia arvude {üks} ja {teine} {tehe}: ");
                    string tulemus = Console.ReadLine();
                    if (int.TryParse(tulemus, out int tulem))
                    {
                        if (vastus == tulem) break;
                    }
                    else
                    {
                        Console.WriteLine("vasta korralikult");
                    }
                    punkte--;
                    if (katse == 4)
                        Console.WriteLine("katsed otsas");
                    else
                        Console.Write($"{katse + 2}. katse - vasta uuesti - ");
                }
            }
            Console.WriteLine($"said kokku {punkte} punkti");
            // Annab hinde kujul suurepärane (45-50), 
            // väga hea(35 - 44), hea(25 - 34), rahuldav(15 - 24), puudulik(5 - 14), 
            // nõrk(0 - 4)

            string[] hinded = { "nõrk", "puudulik", "rahuldav", "hea", "väga hea", "suurepärane" };
            int hinne = (int)Math.Round(punkte / 10.0);
            Console.WriteLine($"sinu hinne on {hinded[hinne]}");


        }
    }
}
